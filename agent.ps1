# CliQr Bundle Service: Puppet
# Tested on Windows 2012
# Expects the following environment variables to already be set by CliQr: 
#   puppet_master_fqdn => FQDN of a Puppet master server to connect and install from.
#   puppet_role        => 'pp_role' when forming the csr.
#   autosigning_psk    => Challenge password when forming the csr.  

param ( 
  $action 
)

$timenow =  get-date -Format "HHmmss"

# Load in CliQr environment variables
$cliqr_userenv_file="c:\temp\userenv.ps1"
& $cliqr_userenv_file

$temp_dir=$env:temp
$puppet_master=$env:puppet_master_fqdn 
$autosigning_psk=$env:autosigning_psk

$puppet_root_dir="C:\Programdata\PuppetLabs\Puppet\etc"
$csr_attributes_file="$puppet_root_dir\csr_attributes.yaml"
$csr_challenge_oid="1.2.840.113549.1.9.7"

write-output "INFO: Executing service script action: '$action'." 

switch ( $action ) {
    install {}
    
    deploy {
      write-output "Creating directories: '$puppet_root_dir' for the CSR file: '$csr_attributes_file'" 
      mkdir $puppet_root_dir
      write-output "INFO: Writing custom CSR attributes in: '$csr_attributes_file'" 
      set-content $csr_attributes_file "custom_attributes:`r`n $($csr_challenge_oid): $autosigning_psk`r`nextension_requests:`r`n pp_role: $puppet_role`r`n" 
      write-output "Executing frictionless installer from the Puppet Master at: '$puppet_master'" 
      [Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
      $webClient = New-Object System.Net.WebClient
      $webClient.DownloadFile("https://$($puppet_master):8140/packages/current/install.ps1", "$env:temp\install-agent.ps1") 
      & "$env:temp\install-agent.ps1" -verbose $true 
      if ((service puppet).status -eq "Running" ) {exit 0} else {exit 1}
    }
    
    configure {}
      
    start {}

    stop {}
    
    restart {}
        
    cleanup {}
          
    reload {}
        
    upgrade {}
        
    default {
      exit 127
    }
}
